# Twilio Example SMS Client


## Building

    go build .

## Running

    SMS_CLIENT_ACCOUNT_SID=... SMS_CLIENT_AUTH_TOKEN=.. SMS_CLIENT_FROM_NUMBER="+111111111" ./m  --to "+19999999999" --body="example message"
