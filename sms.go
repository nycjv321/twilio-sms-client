package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

var ApiEndpoint = "https://api.twilio.com/2010-04-01/Accounts/%v/Messages.json"

type SmsClient struct {
	from           TelephoneNumber
	accountNumber AccountSid
	client         *http.Client
	authentication HeaderAuthenticationProvider
}

type TelephoneNumber = string
type AccountSid = string

type SmsResponse struct {
	Id   string `json:"sid"`
	Body string
}

func (s *SmsClient) send(to TelephoneNumber, body string) (*SmsResponse, error) {
	data := url.Values{}
	data.Set("Body", body)
	data.Set("From", s.from)
	data.Set("To", to)

	client := &http.Client{}
	req, _ := http.NewRequest("POST", fmt.Sprintf(ApiEndpoint, s.accountNumber), strings.NewReader(data.Encode()))

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(data.Encode())))
	req.Header.Add("Authorization", s.authentication.provide())

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			log.Fatal("unable to close response body")
		}
	}()

	if resp.StatusCode == 201 {
		response := SmsResponse{}
		err := json.NewDecoder(resp.Body).Decode(&response)
		return &response, err
	} else {
		bodyRes, err := ioutil.ReadAll(resp.Body)
		return nil, errors.New(fmt.Sprintf("received %v response, see: %v, %v", resp.StatusCode, string(bodyRes), err))
	}
}

type HeaderAuthenticationProvider interface {
	provide() string
}

type AuthTokenAuthenticationProvider struct {
	accountSid string
	authToken  string
}

func (a AuthTokenAuthenticationProvider) provide() string {
	return fmt.Sprintf("Basic %v", base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", a.accountSid, a.authToken))))
}
