package main

import (
  "gopkg.in/alecthomas/kingpin.v2"
)

var (
  body = kingpin.Flag("body", "Message body.").Required().String()
  to   = kingpin.Flag("to", "who the message should be sent to.").Required().String()
)

func ReadArgs() {
  kingpin.Version("0.0.1")
  kingpin.Parse()
}
