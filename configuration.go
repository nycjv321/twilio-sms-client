package main

import (
  "github.com/kelseyhightower/envconfig"
  "log"
)

type Configuration struct {
	AccountSid string `required:"true" split_words:"true"`
  AuthToken  string `required:"true" split_words:"true"`
  FromNumber  string `required:"true" split_words:"true"`
}

func LoadConfiguration() *Configuration {
  var c Configuration
  if err := envconfig.Process("SMS_CLIENT", &c); err == nil {
    return &c
  } else {
    log.Fatalf("unable to load configuration: %v\n", err)
    return &Configuration{"", "", ""}
  }

}
