package main

import (
	"log"
	"net/http"
)

func main() {
  ReadArgs()
  c := LoadConfiguration()
  authentication := &AuthTokenAuthenticationProvider{accountSid: c.AccountSid, authToken: c.AuthToken}
	smsClient := SmsClient{
    from:           c.FromNumber,
		client:         &http.Client{},
		authentication: authentication,
    accountNumber: authentication.accountSid,
	}
	if s, err := smsClient.send(*to, *body); err == nil {
		log.Printf("sms sent: %v\n", s.Id)
	} else {
		log.Fatal(err)
	}

}
